﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace VirtaMed.Unity.Common
{

    public class ObjectSorter<T>
    {
        public interface IComponentContainer
        {
            void MoveDown(T component);
            T[] CollectComponents();
        }
        public interface IDataAccessor
        {
            string GetTypeString(T component);
            bool IsVirtamedType(T component);
        }
       
        private IComponentContainer componentContainer;
        private IDataAccessor dataAccessor;
        public ObjectSorter(IComponentContainer componentContainer, IDataAccessor dataAccessor)
        {
            this.componentContainer = componentContainer;
            this.dataAccessor = dataAccessor;
        }

        public void SortComponents(string separatorObjectName)
        {

            BubbleSort sorter = new BubbleSort(dataAccessor, (a, b) => componentContainer.MoveDown(a), separatorObjectName);
            sorter.Sort(new List<T>(componentContainer.CollectComponents()));
        }

       
        private class BubbleSort 
        {
            private IDataAccessor dataAccessor;
            private Action<T, T> swap;
            private string separatorObjectName;
            public BubbleSort(IDataAccessor dataAccessor, Action<T, T> swap, string separatorObjectName)
            {
                this.dataAccessor = dataAccessor;
                this.swap = swap;
                this.separatorObjectName = separatorObjectName;
            }
            public void Sort(List<T> components)
            {
                bool changed = true;
                while(changed)
                {
                    changed = false;

                    for(int i = 0; i < components.Count - 1; i++)
                    {
                        if (ShouldMoveUp(components[i], components[i + 1]))
                        {
                            BoubleUp(components, i);
                            changed = true;
                        }
                    }
                }
            }
            
            private bool ShouldMoveUp(T a, T b)
            {
                string aType = dataAccessor.GetTypeString(a);
                string bType = dataAccessor?.GetTypeString(b);
                bool aIsVirtamedType = dataAccessor.IsVirtamedType(a);
                bool bIsVirtamedType = dataAccessor.IsVirtamedType(b);
                bool aIsSeparator = aType.Equals(separatorObjectName);
                bool bIsSeparator = bType.Equals(separatorObjectName);
                bool aIsUnityType = aIsVirtamedType == false && aIsSeparator == false;
                bool bIsUnityType = bIsVirtamedType == false && bIsSeparator == false;

                // U: Unity Object
                // V: VirtaMed Object
                // S: Separator
                // 
                // Actions:
                // -: No action
                // U: Bubble Up
                // C: Compare
                //
                // Possibilities: 
                // a:         U | V | U | V | U | V | S | S
                // b:         S | S | U | V | V | U | U | V
                // Action:    - | U | - | C | - | U | U | -

                if (aIsUnityType
                    && bIsUnityType)
                {
                    return false; // Do not make changes on Unity order.
                }
                else if(aIsSeparator && bIsVirtamedType)
                {
                    return false; 
                }
                else if(aIsVirtamedType && bIsSeparator)
                {
                    return true;
                }
                else if(aIsSeparator && bIsUnityType)
                {
                    return true;
                }
                else if(aIsUnityType && bIsSeparator)
                {
                    return false;
                }
                else if(aIsVirtamedType && bIsUnityType)
                {
                    return true;
                }
                else if(aIsUnityType && bIsVirtamedType)
                {
                    return false;
                }
                else 
                {
                    return aType.CompareTo(bType) > 0;
                }
            }

            private void BoubleUp(List<T> components, int i)
            {
                swap(components[i], components[i + 1]);
                components.Reverse(i, 2);
            }
        }

      
        
    }
}
