/*
* (c) 2021 VirtaMed AG - Strictly Confidential - All rights reserved
*
* This document contains unpublished, confidential and proprietary information
* of VirtaMed AG. No disclosure or use of any portion of the contents of these
* materials may be made without the express consent of VirtaMed AG.
*/

using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace VirtaMed.Unity.Common
{
    [DisallowMultipleComponent]
    public class ComponentsSorter : MonoBehaviour
    {

        public void SortComponents<T>(ObjectSorter<T> sorter)
        {
            sorter.SortComponents(GetType().ToString());
        }
    }
}