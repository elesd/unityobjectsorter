using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using VirtaMed.Unity.Common;

public class ComponentSorterTest
{
    private class ComponentEntry
    {
        public ComponentEntry() { }

        public ComponentEntry(int index, string name)
        {
            this.Index = index;
            this.Name = name;
            this.OriginalIndex = index;
        }
        public string Name { get ; set ;  }
        public int Index { get; set; }
        public int OriginalIndex { get; set; }
    }

    private readonly List<string> virtamedComponents = new List<string>
    {
        "FuseCobaComponent",
        "FuseCobaDeformableFixationComponent",
        "FuseCobaLabelFixationComponent",
        "FuseCobaMaterialComponent",
        "FuseCuttableComponent",
        "FuseGraspedBodyComponent",
        "FusePaintableComponent",
        "FuseRenderMeshComponent",
        "FuseTetMeshComponent",
        "FuseTetMeshDistanceFieldComponent",
        "FuseTriMeshComponent",
        "OrganHapticsConfigurator",
        "SoftBodyAnatomy"

    };
    private readonly string separatorComponentName = "ComponentsSorter";

    private readonly List<string> unityComponents = new List<string>
    {
        "BoxCollider",
        "Halo",
        "MeshRenderer",
        "SomethingFunky"
    };

    List<ComponentEntry> GenerateTestData(List<string> componentNames)
    {
        List<ComponentEntry> result = new List<ComponentEntry>();
        foreach (var componentName in componentNames)
        {
            ComponentEntry entry = new ComponentEntry(result.Count, componentName);
            result.Add(entry);
        }
        return result;
    }

     private class TestComponentContainer : ObjectSorter<ComponentEntry>.IComponentContainer
     {
        private List<ComponentEntry> components;
        public TestComponentContainer(List<ComponentEntry> components)
        {
            this.components = components;
        }
        public void MoveDown(ComponentEntry component)
        {
            var nextComponent = components[component.Index + 1];
            components.Reverse(component.Index, 2);

            component.Index = component.Index + 1;
            nextComponent.Index = nextComponent.Index - 1;
        }
        public ComponentEntry[] CollectComponents()
        {
            return components.ToArray();
        }
    }

    private class TestDataAccessor : ObjectSorter<ComponentEntry>.IDataAccessor
    {
        public string GetTypeString(ComponentEntry entry)
        {
            return entry.Name;
        }
        public bool IsVirtamedType(ComponentEntry entry)
        {
            var componentType = GetTypeString(entry);
            return componentType.Contains("Virta") ||
                        componentType.Contains("Fuse") ||
                        componentType.Contains("SoftBody") ||
                        componentType.Contains("OrganHaptics") ||
                        componentType.Contains("ICG");
        }
    }


    [Test]
    public void TestNoSortingOnlyVirtamed()
    {
        var components = new List<string>(virtamedComponents);
        components.Insert(0, separatorComponentName);
        var inputData = GenerateTestData(components);

        TestComponentContainer container = new TestComponentContainer(inputData);

        var sorter = new ObjectSorter<ComponentEntry>(container, new TestDataAccessor());
        sorter.SortComponents(separatorComponentName);
        foreach(var component in inputData)
        {
            Assert.AreEqual(component.OriginalIndex, component.Index);
        }
    }

    [Test]
    public void TestSortingOnlyVirtamed()
    {
        var components = new List<string>(virtamedComponents);

        components.Insert(0, separatorComponentName);
        components.Reverse(1, 2);

        var inputData = GenerateTestData(components);

        TestComponentContainer container = new TestComponentContainer(inputData);

        var sorter = new ObjectSorter<ComponentEntry>(container, new TestDataAccessor());
        sorter.SortComponents(separatorComponentName);
        Assert.AreEqual(0, inputData[0].OriginalIndex);
        Assert.AreEqual(2, inputData[1].OriginalIndex);
        Assert.AreEqual(1, inputData[2].OriginalIndex);
        for (int i = 3; i < inputData.Count - 1; ++i)
        {
            var component = inputData[i];
            Assert.AreEqual(component.Index, component.OriginalIndex);
        }
    }

    [Test]
    public void TestStableSortingOnlyVirtamed()
    {
        var components = new List<string>(virtamedComponents);
        components.Insert(0, separatorComponentName);
        components.Add(components[components.Count - 1]);

        var inputData = GenerateTestData(components);

        TestComponentContainer container = new TestComponentContainer(inputData);

        var sorter = new ObjectSorter<ComponentEntry>(container, new TestDataAccessor());
        sorter.SortComponents(separatorComponentName);

        for (int i = 0; i < inputData.Count; ++i)
        {
            var component = inputData[i];
            Assert.AreEqual(component.OriginalIndex, component.Index);
        }
    }

    [Test]
    public void TestSeparabilityEasy()
    {
        var components = new List<string>(unityComponents);
        
        components.Add(separatorComponentName);

        components.AddRange(virtamedComponents);
        
        var inputData = GenerateTestData(components);

        TestComponentContainer container = new TestComponentContainer(inputData);

        var sorter = new ObjectSorter<ComponentEntry>(container, new TestDataAccessor());
        sorter.SortComponents(separatorComponentName);

        for (int i = 0; i < inputData.Count; ++i)
        {
            var component = inputData[i];
            Assert.AreEqual(component.OriginalIndex, component.Index);
        }
    }
    [Test]
    public void TestSeparabilitySwap()
    {
        var components = new List<string>(virtamedComponents);

        components.Add(separatorComponentName);

        components.AddRange(unityComponents);

        var inputData = GenerateTestData(components);

        TestComponentContainer container = new TestComponentContainer(inputData);

        var sorter = new ObjectSorter<ComponentEntry>(container, new TestDataAccessor());
        sorter.SortComponents(separatorComponentName);

        for (int i = 0; i < unityComponents.Count; ++i)
        {
            var component = inputData[i];
            Assert.IsTrue(unityComponents.IndexOf(component.Name) >= 0);
        }
        Assert.AreEqual(inputData[unityComponents.Count].Name, separatorComponentName);
        for(int i = unityComponents.Count + 1; i < inputData.Count; i++)
        {
            var component = inputData[i];
            Assert.AreEqual(component.OriginalIndex + unityComponents.Count + 1, component.Index);
        }
    }

    [Test]
    public void TestSeparabilitySwapAndSort()
    {
        var components = new List<string>(virtamedComponents);
        components.Reverse(0, 2);

        components.Add(separatorComponentName);

        components.AddRange(unityComponents);

        var inputData = GenerateTestData(components);

        TestComponentContainer container = new TestComponentContainer(inputData);

        var sorter = new ObjectSorter<ComponentEntry>(container, new TestDataAccessor());
        sorter.SortComponents(separatorComponentName);

        for (int i = 0; i < unityComponents.Count; ++i)
        {
            var component = inputData[i];
            Assert.IsTrue(unityComponents.IndexOf(component.Name) >= 0);
        }
        Assert.AreEqual(inputData[unityComponents.Count].Name, separatorComponentName);
        int firstIndex = unityComponents.Count + 1;
        {
            var firstComponent = inputData[firstIndex];
            Assert.AreEqual(firstComponent.OriginalIndex + firstIndex - 1, firstComponent.Index);
            var secondComponent = inputData[firstIndex + 1];
            Assert.AreEqual(secondComponent.OriginalIndex + firstIndex + 1, secondComponent.Index);
        }
        for (int i = firstIndex + 2; i < inputData.Count; i++)
        {
            var component = inputData[i];
            Assert.AreEqual(component.OriginalIndex + firstIndex, component.Index);
        }
    }

 }
